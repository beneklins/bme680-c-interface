/*
 * This file is part of “BME680 C interface”. For the licence, see the
 * LICENCE file.
 */

/*
 * Read the BME680 sensor with the BSEC library by running an endless loop in
 * the bsec_iot_loop() function under Linux.
 *
 */

/*#define _POSIX_C_SOURCE 200809L*/
#define _XOPEN_SOURCE 700

/* Header files */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/i2c-dev.h>
#include "bsec_integration.h"

/* Definitions */
#define DESTZONE "TZ=America/Toronto"
#define temp_offset (0.0f)
#define sample_rate_mode (BSEC_SAMPLE_RATE_LP)

// I2C Linux device handle.
int g_i2cFid;
int i2c_address = BME680_I2C_ADDR_PRIMARY;
/* IAQ configuration and state files */
char *filename_state = "bsec_iaq.state";
char *filename_config = "bsec_iaq.config";

/* Functions */

// Open the Linux device.
void i2cOpen()
{
  g_i2cFid = open("/dev/i2c-1", O_RDWR);
  if (g_i2cFid < 0) {
    perror("i2cOpen");
    exit(1);
  }
}

// Close the Linux device.
void i2cClose()
{
  close(g_i2cFid);
}

// Set the I2C slave address for all subsequent I2C device transfers.
void i2cSetAddress(int address)
{
  if (ioctl(g_i2cFid, I2C_SLAVE, address) < 0) {
    perror("i2cSetAddress");
    exit(1);
  }
}

/*
 * Write operation in either I2C or SPI
 *
 * param[in]        dev_addr        I2C or SPI device address
 * param[in]        reg_addr        Register address
 * param[in]        reg_data_ptr    Pointer to the data to be written
 * param[in]        data_len        Number of bytes to be written
 *
 * return           Result of the bus communication function
 */
int8_t bus_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data_ptr,
                 uint16_t data_len)
{
  // Return 0 for success and non-zero for failure.
  int8_t rslt = 0;

  uint8_t reg[16];
  reg[0]=reg_addr;
  int i;

  for (i=1; i<data_len+1; i++)
    reg[i] = reg_data_ptr[i-1];

  if (write(g_i2cFid, reg, data_len+1) != data_len+1) {
    perror("user_i2c_write");
    rslt = 1;
    exit(1);
  }

  return rslt;
}

/*
 * Read operation in either I2C or SPI
 *
 * param[in]        dev_addr        I2C or SPI device address
 * param[in]        reg_addr        Register address
 * param[out]       reg_data_ptr    Pointer to the memory to be used to store
 *                                  the read data
 * param[in]        data_len        Number of bytes to be read
 *
 * return           Result of the bus communication function
 */
int8_t bus_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data_ptr,
                uint16_t data_len)
{
  // Return 0 for success and non-zero for failure.
  int8_t rslt = 0;

  uint8_t reg[1];
  reg[0]=reg_addr;

  if (write(g_i2cFid, reg, 1) != 1) {
    perror("user_i2c_read_reg");
    rslt = 1;
  }

  if (read(g_i2cFid, reg_data_ptr, data_len) != data_len) {
    perror("user_i2c_read_data");
    rslt = 1;
  }

  return rslt;
}

/*
 * System specific implementation of sleep function
 *
 * param[in]       t_ms    Time in milliseconds
 *
 * return          none
 */
void _sleep(uint32_t t_ms)
{
  struct timespec ts;
  ts.tv_sec = 0;
  // mod because nsec must be in the range 0 to 999999999.
  ts.tv_nsec = (t_ms % 1000) * 1000000L;
  nanosleep(&ts, NULL);
}

/*
 * Capture the system time in microseconds
 *
 * return          system_current_time    System timestamp in microseconds
 */
int64_t get_timestamp_us()
{
  struct timespec spec;
  // Use CLOCK_MONOTONIC to avoid interference by time sync.
  clock_gettime(CLOCK_MONOTONIC, &spec);

  int64_t system_current_time_ns = (int64_t)(spec.tv_sec) * (int64_t)1000000000
                                   + (int64_t)(spec.tv_nsec);
  int64_t system_current_time_us = system_current_time_ns / 1000;

  return system_current_time_us;
}

/*
 * Handling of the ready outputs
 *
 * param[in]       timestamp       Time in microseconds
 * param[in]       iaq             IAQ signal
 * param[in]       iaq_accuracy    Accuracy of IAQ signal
 * param[in]       temperature     Temperature signal
 * param[in]       humidity        Humidity signal
 * param[in]       pressure        Pressure signal
 * param[in]       raw_temperature Raw temperature signal
 * param[in]       raw_humidity    Raw humidity signal
 * param[in]       gas             Raw gas sensor signal
 * param[in]       bsec_status     Value returned by the bsec_do_steps() call
 * param[in]       static_iaq      Unscaled indoor-air-quality estimate
 * param[in]       co2_equivalent  CO2 equivalent estimate [ppm]
 * param[in]       breath_voc_equivalent  Breath VOC concentration estimate [ppm]
 *
 * return          none
 */
void output_ready(int64_t timestamp, float iaq, uint8_t iaq_accuracy,
                  float temperature, float humidity, float pressure,
                  float raw_temperature, float raw_humidity, float gas,
                  bsec_library_return_t bsec_status,
                  float static_iaq, float co2_equivalent,
                  float breath_voc_equivalent)
{

  /*
   * Timestamp for localtime only makes sense if get_timestamp_us() uses
   * CLOCK_REALTIME
   */
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);

  // bsec_do_steps return code.
  printf("%d,", bsec_status);
  // Date.
  printf("%d-%02d-%02d,", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
  // Local time.
  printf("%02d:%02d:%02d,", tm.tm_hour, tm.tm_min, tm.tm_sec);
  // IAQ accuracy status: 0-3.
  printf("%d,", iaq_accuracy);
  // IAQ: 0-500, resolution of 1, auto-trim algorithm (for mobile devices)
  printf("%.1f,", iaq);
  // Static IAQ: 0-500, resolution of 1, no auto-trim algorithm (for
  // non-mobile devices)
  // TODO: Check Static IAQ ranges and resolution.
  printf("%.1f,", static_iaq);
  // Sensor-compensated temperature: -40-85°C, accuracy ±1.0°C.
  printf("%.1f,", temperature);
  // Sensor-compensated relative humidity: 0-100%, accuracy ±3%.
  printf("%.1f,", humidity);
  // Raw pressure: 300‒1100 hPa.
  //  Relative accuracy ±0.12 hPa ≈ ±1 m (altitude).
  //  Absolute accuracy ±0.6 hPa ≈ ±5 m (altitude).
  printf("%.2f,", pressure/100);
  // Sensor-compensated gas sensor resistance in ohms.
  printf("%.0f,", gas);
  // Estimation of the CO2 level in ppm.
  printf("%.15f,", co2_equivalent);
  // Estimation of breath-VOC equivalents in ppm.
  printf("%.25f,", breath_voc_equivalent);
  printf("\r\n");
  fflush(stdout);
}

/*
 * Load binary file from non-volatile memory into buffer.
 *
 * param[in, out]  state_buffer    Buffer to hold the loaded data
 * param[in]       n_buffer        Size of the allocated buffer
 * param[in]       filename        Name of the file on the NVM
 * param[in]       offset          Offset in bytes from where to start copying
 *                                  to buffer
 * return          Number of bytes copied to buffer or zero on failure
 */
uint32_t binary_load(uint8_t *b_buffer, uint32_t n_buffer, char *filename,
                     uint32_t offset)
{
  int32_t copied_bytes = 0;
  int8_t rslt = 0;

  struct stat fileinfo;
  rslt = stat(filename, &fileinfo);
  if (rslt != 0) {
    fprintf(stderr, "Stat'ing binary file %s: ", filename);
    perror("");
    return 0;
  }

  uint32_t filesize = fileinfo.st_size - offset;

  if (filesize > n_buffer) {
    fprintf(stderr,"%s: %d > %d\n", "Binary data bigger than buffer", filesize,
            n_buffer);
    return 0;
  } else {
    FILE *file_ptr;
    file_ptr = fopen(filename,"rb");
    if (!file_ptr) {
      perror("fopen");
      return 0;
    }
    fseek(file_ptr,offset,SEEK_SET);
    copied_bytes = fread(b_buffer,sizeof(char),filesize,file_ptr);
    if (copied_bytes == 0) {
      fprintf(stderr, "%s empty\n", filename);
    }
    fclose(file_ptr);
    return copied_bytes;
  }
}

/*
 * Load previous library state from non-volatile memory.
 *
 * param[in,out]   state_buffer    Buffer to hold the loaded state string
 * param[in]       n_buffer        Size of the allocated state buffer
 *
 * return          Number of bytes copied to state_buffer or zero on failure
 */
uint32_t state_load(uint8_t *state_buffer, uint32_t n_buffer)
{
  int32_t rslt = 0;
  rslt = binary_load(state_buffer, n_buffer, filename_state, 0);
  return rslt;
}

/*
 * Save library state to non-volatile memory.
 *
 * param[in]       state_buffer    Buffer holding the state to be stored
 * param[in]       length          Length of the state string to be stored
 *
 * return          none
 */
void state_save(const uint8_t *state_buffer, uint32_t length)
{
  FILE *state_w_ptr;
  state_w_ptr = fopen(filename_state, "wb");
  fwrite(state_buffer, length, 1, state_w_ptr);
  fclose(state_w_ptr);
}

/*
 * Load library configuration from non-volatile memory.
 *
 * param[in,out]   config_buffer    Buffer to hold the loaded state string
 * param[in]       n_buffer         Size of the allocated state buffer
 *
 * return          Number of bytes copied to config_buffer or zero on failure
 */
uint32_t config_load(uint8_t *config_buffer, uint32_t n_buffer)
{
  int32_t rslt = 0;
  /*
   * Provided configuration file is 4 bytes larger than buffer.
   * Apparently skipping the first 4 bytes works fine.
   *
   */
  rslt = binary_load(config_buffer, n_buffer, filename_config, 4);
  return rslt;
}

/*
 * Main function which configures the BSEC library and then reads and
 * processes the data from sensor based on timer ticks.
 *
 * return      Result of the processing
 */
int main()
{
  // Switch to destination time zone
  putenv(DESTZONE);

  i2cOpen();
  i2cSetAddress(i2c_address);

  return_values_init ret;

  ret = bsec_iot_init(sample_rate_mode, temp_offset, bus_write, bus_read,
                      _sleep, state_load, config_load);
  if (ret.bme680_status) {
    // Could not initialise the BME680 sensor.
    return (int)ret.bme680_status;
  } else if (ret.bsec_status) {
    // Could not initialise the BSEC library.
    return (int)ret.bsec_status;
  }

  /*
   * Call to endless loop function which reads and processes data based on
   * sensor settings.
   * State is saved every 10000 samples, which means every
   * 10000 * 3 secs = 500 minutes (depending on the configuration).
   *
   */
  bsec_iot_loop(_sleep, get_timestamp_us, output_ready, state_save, 10000);
  fflush(stdout);

  i2cClose();
  return 0;
}

